module codeberg.org/gruf/gem

go 1.17

require (
	codeberg.org/gruf/go-bytes v1.0.2
	codeberg.org/gruf/go-config v1.0.0
	codeberg.org/gruf/go-fastnet v1.3.1
	codeberg.org/gruf/go-fastpath v1.0.2
	codeberg.org/gruf/go-format v1.0.6
	codeberg.org/gruf/go-hashenc v1.0.2
	codeberg.org/gruf/go-logger v1.5.1
	codeberg.org/gruf/go-pools v1.0.2
	codeberg.org/gruf/go-uri v1.0.5
)

require (
	codeberg.org/gruf/go-ctx v1.0.2 // indirect
	codeberg.org/gruf/go-errors v1.0.5 // indirect
	codeberg.org/gruf/go-mimetypes v1.0.0 // indirect
	codeberg.org/gruf/go-nowish v1.1.0 // indirect
	github.com/pelletier/go-toml v1.9.4 // indirect
)
