package main

import (
	"crypto/sha256"
	"net"
	"os"
	"time"

	"codeberg.org/gruf/go-config"
	"codeberg.org/gruf/go-fastnet"
	"codeberg.org/gruf/go-fastnet/gemini"
	"codeberg.org/gruf/go-fastpath"
	"codeberg.org/gruf/go-format"
	"codeberg.org/gruf/go-hashenc"
	"codeberg.org/gruf/go-logger"
	"codeberg.org/gruf/go-pools"
)

const (
	name    = "gem"
	version = "v0.1.1"
)

var (
	// defaultCGIEnv stores the base CGI environment
	// variables that will be built-upon for each CGI request.
	defaultCGIEnv []string

	// gemini server dir paths.
	cgiDir  = "/cgi-bin"
	rootDir = ""

	// stored gemini server hostname (& with port).
	hostname         = ""
	hostnamePlusPort = ""

	// log is the global log instance
	log = logger.NewWith(
		0,
		true,
		logger.Format(),
		512, // entry buf
		os.Stdout,
	)

	// henc is the global sha256-hex hash encoder (for CGI TLS hashes).
	henc = hashenc.New(sha256.New(), hashenc.Hex())

	// bufPool is the global buffer pool.
	bufPool = pools.NewBufferPool(512)
)

// usage simply prints binary usage.
func usage(code int) {
	format.Printf("Usage: {} [-c|--config $file]\n", os.Args[0])
	os.Exit(code)
}

func main() {
	// Default configuration file location
	configFile := "/etc/gem.toml"

	// If we have arguments to handle, do so!
	if len(os.Args) > 1 {
		switch os.Args[1] {
		case "-c", "--config":
			if len(os.Args) != 3 {
				usage(1)
			}
			configFile = os.Args[2]
		default:
			usage(1)
		}
	}

	// Parse runtime flags from config
	config.StringVar(&hostname, "hostname", "")
	bindAddr := config.String("listen", "0.0.0.0:1965")
	tlsCert := config.String("tls-cert", "")
	tlsKey := config.String("tls-key", "")
	root := config.String("root-dir", "Server root dir")
	cgiPath := config.String("cgi-path", "/bin:/usr/bin")
	restrict := config.StringArray("restricted", []string{})
	readTimeout := config.Duration("read-timeout", time.Second*10)
	writeTimeout := config.Duration("write-timeout", 0)
	allowCerts := config.StringArray("allow-certs", []string{})
	config.Parse(configFile)

	// Check for valid bind-addr
	if len(*bindAddr) < 1 {
		log.Fatal("bind-addr must not be empty")
	}
	host, port, err := net.SplitHostPort(*bindAddr)
	if err != nil {
		log.Fatal("invalid bind-addr (does it contain port?)")
	}

	// Ensure hostname versions are set
	if len(hostname) < 1 {
		hostname = host
		hostnamePlusPort = host + ":" + port
	} else {
		// Check if hostname contains port
		host, _, err = net.SplitHostPort(hostname)
		if err == nil {
			// Contains port, remove port from host
			hostnamePlusPort = hostname
			hostname = host
		} else {
			// Contains no port, add port string
			hostnamePlusPort = hostname + ":" + port
		}
	}

	// Check hostname is valid (no path, query, etc)
	if !isValidHostname(hostname) || !isValidHostname(hostnamePlusPort) {
		log.Fatal("invalid hostname")
	}

	// Check for TLS files
	if len(*tlsCert) < 1 {
		log.Fatal("tls-cert must not be empty")
	}
	if len(*tlsKey) < 1 {
		log.Fatal("tls-key must not be empty")
	}

	// Get parsed root dir
	rootDir = (&fastpath.Builder{}).Clean(*root)

	// Prepare default CGI env
	defaultCGIEnv = []string{
		"GATEWAY_INTERFACE=CGI/1.1",
		"SERVER_SOFTWARE=" + name + " " + version,
		"SERVER_PROTOCOL=gemini",
		"SERVER_NAME=" + hostname,
		"SERVER_PORT=" + port,
		"DOCUMENT_ROOT=" + rootDir,
		"PATH=" + *cgiPath,
	}

	// Create mux and add restricted
	mux := fastnet.Mux{}
	for _, path := range *restrict {
		mux.Handle(path, gemini.HandlerFunc(HandleRestricted))
	}

	// Finally add default routes
	mux.Handle("/cgi-bin", gemini.HandlerFunc(HandleRestricted))
	mux.Handle("/cgi-bin/:path", gemini.HandlerFunc(HandleCGI))
	mux.RootHandler = gemini.HandlerFunc(HandleFiles)
	mux.PanicHandler = PanicHandler

	// Add TLS cert blocking if necessary
	handler := (fastnet.Handler)(&mux)
	if len(*allowCerts) > 0 {
		mapped := make(map[string]struct{}, len(*allowCerts))

		for _, certFile := range *allowCerts {
			// Load TLS cert from file
			cert, err := loadTLSCert(certFile)
			if err != nil {
				log.Fatalf("failed reading TLS cert: {}", err)
			}

			// Hash raw cert data and map
			hashed := henc.EncodedSum(cert.Raw).String()
			mapped[hashed] = struct{}{}
		}

		// Setup authorization handler with mapped cert hashes
		handler = AuthorizationHandler(&mux, mapped)
	}

	// Setup and create server
	srv := gemini.NewServer()
	srv.ReadTimeout = *readTimeout
	srv.WriteTimeout = *writeTimeout
	srv.LogOut = os.Stdout
	srv.Handler = LoggingHandler(handler)

	// Start listening
	log.Infof("Listening on gemini://{}", *bindAddr)
	err = srv.ListenAndServeTLS("tcp", *bindAddr, *tlsCert, *tlsKey)
	log.Error(err)
}
