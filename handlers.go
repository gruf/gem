package main

import (
	"crypto/tls"
	"crypto/x509"
	"os"
	"os/exec"
	"runtime"
	"strings"
	"time"

	"codeberg.org/gruf/go-bytes"
	"codeberg.org/gruf/go-fastnet"
	"codeberg.org/gruf/go-fastnet/gemini"
	"codeberg.org/gruf/go-format"
	"codeberg.org/gruf/go-logger"
)

// Mapped to tls.Version__ where:
// value - 0x300 = index.
// Deprecated versions will always be
// prefixed by 'DEPRECATED'
var tlsVersionStrings = [5]string{
	tls.VersionSSL30 - 0x300: "DEPRECATED:SSLv3",
	tls.VersionTLS10 - 0x300: "TLSv1.0",
	tls.VersionTLS11 - 0x300: "TLSv1.1",
	tls.VersionTLS12 - 0x300: "TLSv1.2",
	tls.VersionTLS13 - 0x300: "TLSv1.3",
}

func LoggingHandler(h fastnet.Handler) gemini.HandlerFunc {
	return func(ctx *fastnet.RequestCtx, rw gemini.ResponseWriter, r *gemini.Request) {
		// Start timer + defer log
		before := time.Now()
		defer func() {
			// Stop timing
			diff := time.Since(before)

			// Create and send log
			entry := log.Entry()
			entry = entry.Timestamp()
			entry = entry.Appendf(
				"address={:v} requestURI={:v} statusCode={:v} statusMeta={:v} duration={:v}",
				ctx.RemoteAddr().String(),
				ctx.URI().RequestURI().StringPtr(),
				rw.Status(),
				rw.Meta(),
				diff,
			)

			// Print log entry
			entry.Send()
		}()

		// Check this is to expected gemini host
		host := ctx.URI().Host().StringPtr()
		if host != hostname && host != hostnamePlusPort {
			rw.WriteHeader(gemini.StatusProxyRequestRefused, "Unknown Hostname")
			return
		}

		// Pass to serve mux
		h.Serve(ctx)
	}
}

func PanicHandler(ctx *fastnet.RequestCtx, i interface{}) {
	// Prepare a stacktrace
	buf := format.Buffer{B: make([]byte, 0, 1024)}
	buf.AppendString("panic stacktrace:\n")

	pcs := make([]uintptr, 10)
	depth := runtime.Callers(0, pcs)
	frames := runtime.CallersFrames(pcs[:depth])
	for f, again := frames.Next(); again; f, again = frames.Next() {
		format.Appendf(&buf, "\t{} #{}: {}\n", f.File, f.Line, f.Function)
	}

	// Sneakily use 1 entry for two different log
	// entries so they get buffered and printed togeher
	entry := log.Entry()
	entry.TimestampIf().Level(logger.ERROR).Appendf("caught panic: %v\n", i)
	entry.TimestampIf().Level(logger.ERROR).Append(buf.String())
	entry.Send()

	// Ensure status header is written
	rsp := gemini.GetResponseWriter(ctx)
	if rsp.Status() == 0 {
		rsp.WriteHeader(gemini.StatusTemporaryFailure, gemini.StatusText(gemini.StatusTemporaryFailure))
	}
}

func HandleRestricted(ctx *fastnet.RequestCtx, rw gemini.ResponseWriter, r *gemini.Request) {
	rw.WriteHeader(gemini.StatusNotFound, "Forbidden")
}

func HandleCGI(ctx *fastnet.RequestCtx, rw gemini.ResponseWriter, r *gemini.Request) {
	// Calculate total path, both are cleaned and URI path
	// is rooted, so safe to join as-so
	totalPath := rootDir + ctx.URI().Path().StringPtr()

	// Start index of URI segment
	uriStart := len(rootDir)

	// End index of CGI directory segment
	cgiEnd := uriStart + len(cgiDir)

	// upTo is the beginning index we iter and index search from
	upTo := cgiEnd

	// pathEnd tracks the last valid (i.e. non-stat-error path
	// calculated from os.Stat)
	pathEnd := len(totalPath)

	for {
		// Get start of next path segment
		next := strings.IndexByte(totalPath[upTo+1:], '/')
		if next == -1 {
			break
		}

		// Iterate up-to
		upTo += next

		// Check file on disk
		stat, err := os.Stat(totalPath[:upTo+1])
		if err != nil {
			break
		}

		// Not dir, set last valid and break
		if !stat.IsDir() {
			pathEnd = upTo + 1
			break
		}
	}

	// Create cmd object
	cmd := exec.CommandContext(ctx, totalPath[:pathEnd])

	// Start preparing CGI env
	env := append(defaultCGIEnv, []string{
		"TLS_CIPHER=" + tls.CipherSuiteName(r.TLS().CipherSuite),
		"TLS_VERSION" + tlsVersionStrings[r.TLS().Version-0x300],
		"GEMINI_URL=" + ctx.URI().FullURI().StringPtr(),
		"REMOTE_ADDR=" + ctx.URI().Host().StringPtr(),
		"QUERY_STRING=" + ctx.URI().RawQuery().StringPtr(),
		"REQUEST_URI=" + ctx.URI().RequestURI().StringPtr(),
		"SCRIPT_NAME=" + totalPath[uriStart+1:pathEnd],
		"SCRIPT_FILENAME=" + totalPath[:pathEnd],
		"PATH_INFO=" + totalPath[pathEnd:],
	}...)

	// If client certs provided, set information
	if len(r.TLS().PeerCertificates) > 0 {
		// We don't support multiple certs
		cert := r.TLS().PeerCertificates[0]

		// Verify client cert
		var isVerified string
		_, err := cert.Verify(x509.VerifyOptions{})
		if err != nil {
			isVerified = "FAILED:" + err.Error()
		} else {
			isVerified = "SUCCESS"
		}

		// Check if hash already calcuated, else calculate
		hash, ok := ctx.Value("tlsHash").(bytes.Bytes)
		if !ok {
			hash = henc.EncodedSum(cert.Raw)
			ctx.WithValue("tlsHash", hash)
		}

		// Set user cert environment vars
		env = append(env, "AUTH_TYPE=Certificate")
		env = append(env, "REMOTE_USER="+cert.Subject.CommonName)
		env = append(env, "TLS_CLIENT_HASH=SHA256:"+hash.StringPtr())
		env = append(env, "TLS_CLIENT_NOT_BEFORE="+cert.NotBefore.String())
		env = append(env, "TLS_CLIENT_NOT_AFTER="+cert.NotAfter.String())
		env = append(env, "TLS_CLIENT_ISSUER="+cert.Issuer.String())
		env = append(env, "TLS_CLIENT_SUBJECT="+cert.Subject.String())
		env = append(env, "TLS_CLIENT_VERIFIED="+isVerified)
	}

	// Setup cmd environment
	cmd.Env = env
	cmd.Dir = rootDir

	// Setup cmd out writer
	cmd.Stdout = rw
	cmd.Stderr = nil

	// Start executing
	err := cmd.Start()
	if err != nil {
		rw.WriteHeader(gemini.StatusNotFound, gemini.StatusText(gemini.StatusNotFound))
		return
	}

	// Wait for command to finish
	err = cmd.Wait()
	if err != nil {
		log.Error(err)
	}
}

func HandleFiles(ctx *fastnet.RequestCtx, rw gemini.ResponseWriter, r *gemini.Request) {
	// Calculate filepath, both are cleaned and URI
	// path is rooted, so safe to join as-so
	filepath := rootDir
	uripath := ctx.URI().Path().StringPtr()
	if len(uripath) > 1 {
		filepath += uripath
	}

	// Check file on disk
	stat, err := os.Stat(filepath)
	if err != nil {
		rw.WriteHeader(gemini.StatusNotFound, gemini.StatusText(gemini.StatusNotFound))
		return
	}

	// Attempt to open dir index page, or failing
	// that serve a generated directory listing
	var file *os.File
	if stat.Mode().IsDir() {
		file, err = os.OpenFile(filepath+"/index.gmi", os.O_RDONLY, 0400)
		if err != nil {
			// Error opening index. Open dir
			file, err = os.OpenFile(filepath, os.O_RDONLY, 0700)
			if err != nil {
				rw.WriteHeader(gemini.StatusNotFound, gemini.StatusText(gemini.StatusNotFound))
				return
			}

			// Read directory contents
			entries, err := file.ReadDir(-1)
			if err != nil {
				rw.WriteHeader(gemini.StatusNotFound, gemini.StatusText(gemini.StatusNotFound))
				return
			}

			// Acquire buffer for writing
			buf := bufPool.Get()

			// Write gem header + dir listing heading
			buf.WriteString("20 text/gemini\r\n")
			buf.WriteString("# ")
			buf.Write(ctx.URI().Path().Bytes())
			buf.WriteString("\n\n")

			// Get formatted relative path
			relPath := uripath
			if len(uripath) < 2 {
				relPath = ""
			}

			// Write backtrack path
			buf.WriteString("=> ")
			buf.WriteString(relPath)
			buf.WriteString("/.. ../\n")

			// Iter and print entries
			for _, entry := range entries {
				// Generate entry name
				name := entry.Name()
				if entry.IsDir() {
					name += "/"
				}

				// Write generated dir entry line
				buf.WriteString("=> ")
				buf.WriteString(relPath)
				buf.WriteByte('/')
				buf.WriteString(name)
				buf.WriteByte(' ')
				buf.WriteString(name)
				buf.WriteByte('\n')
			}

			// Write buffer to client
			rw.Write(buf.B)
			bufPool.Put(buf)

			return
		}
	} else {
		// Open supplied file path
		file, err = os.OpenFile(filepath, os.O_RDONLY, 0400)
		if err != nil {
			rw.WriteHeader(gemini.StatusNotFound, "Not Found")
			return
		}
	}

	// Serve file + handle mimetype detection
	gemini.ServeFile(ctx, rw, r, file)
}

func AuthorizationHandler(h fastnet.Handler, allowCerts map[string]struct{}) gemini.HandlerFunc {
	return func(ctx *fastnet.RequestCtx, rw gemini.ResponseWriter, r *gemini.Request) {
		if len(r.TLS().PeerCertificates) > 0 {
			// Grab the client certificate and hash it
			cert := r.TLS().PeerCertificates[0]
			hash := henc.EncodedSum(cert.Raw)
			hashStr := hash.StringPtr()

			// Store hash in ctx to prevent future rehashing
			ctx.WithValue("tlsHash", hash)

			// Check if this hash is permitted
			_, permit := allowCerts[hashStr]

			// Only pass to handler if permitted
			if permit {
				h.Serve(ctx)
				return
			}

			// Store error for logging with cert hash
			log.Error("unpermitted cert hash: " + hashStr)
		}

		// Default is to bar entry
		rw.WriteHeader(gemini.StatusNotFound, "Unpermitted Client")
	}
}
