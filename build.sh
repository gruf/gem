#!/bin/sh

CGO_ENABLED=0 go build -trimpath -a -v -tags 'netgo osusergo static_build' -ldflags '-s -w -extldflags "-static"' -gcflags '-l=4'
