#!/bin/sh

[ $# -ne 1 ] && {
    echo "Usage: ${0} <hostname>"
    exit 1
}

echo "Generating TLS cert + privkey for: ${1}"
openssl req -x509 -newkey rsa:4096 -sha256 \
            -days 365 -nodes -keyout "${1}.key" \
            -out "${1}.crt" -subj "/CN=${1}" 
#            -addext "subjectAltName=DNS:${1}"