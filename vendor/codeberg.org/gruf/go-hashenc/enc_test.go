package hashenc_test

import (
	"bytes"
	"encoding/base32"
	"encoding/base64"
	"encoding/hex"
	"testing"

	"codeberg.org/gruf/go-hashenc"
)

func TestBase32(t *testing.T) {
	testEncoder(t, hashenc.Base32(), func(expect, src []byte) bool {
		enc := base32.StdEncoding.WithPadding(base64.NoPadding)
		dst := make([]byte, enc.EncodedLen(len(src)))
		enc.Encode(dst, src)
		return bytes.Equal(expect, dst)
	})
}

func TestBase64(t *testing.T) {
	testEncoder(t, hashenc.Base64(), func(expect, src []byte) bool {
		enc := base64.StdEncoding.WithPadding(base64.NoPadding)
		dst := make([]byte, enc.EncodedLen(len(src)))
		enc.Encode(dst, src)
		return bytes.Equal(expect, dst)
	})
}

func TestHex(t *testing.T) {
	testEncoder(t, hashenc.Hex(), func(expect, src []byte) bool {
		dst := make([]byte, hex.EncodedLen(len(src)))
		hex.Encode(dst, src)
		return bytes.Equal(expect, dst)
	})
}

func testEncoder(t *testing.T, enc hashenc.Encoder, test func(expect []byte, src []byte) bool) {
	if enc == nil {
		t.Fatal("Nil Encoder")
	}

	src := []byte("hello world")
	dst := make([]byte, enc.EncodedLen(len(src)))
	enc.Encode(dst, src)

	if !test(dst, src) {
		t.Fatal("Encoded did not match expected")
	}
}
