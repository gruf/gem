package hashenc_test

import (
	"bytes"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
	"hash"
	"testing"

	"codeberg.org/gruf/go-hashenc"
)

func TestHashEncoder(t *testing.T) {
	for _, hash := range []hash.Hash{sha256.New(), sha512.New(), sha1.New()} {
		testHashEncoderPair(t, hash, hashenc.Base64())
		testHashEncoderPair(t, hash, hashenc.Base32())
		testHashEncoderPair(t, hash, hashenc.Hex())
	}
}

func testHashEncoderPair(t *testing.T, hash hash.Hash, enc hashenc.Encoder) {
	henc := hashenc.New(hash, enc)
	if henc == nil {
		t.Fatal("Nil HashEncoder")
	}

	b := []byte("hello world")
	sum := henc.EncodedSum(b)

	hash.Reset()
	hash.Write(b)
	hashed := hash.Sum([]byte{})
	encode := make([]byte, enc.EncodedLen(len(hashed)))
	enc.Encode(encode, hashed)

	if !bytes.Equal(sum.Bytes(), encode) {
		t.Fatalf("Encoded hash sum did not match expected: hash=%T sum='%s' expect='%s'", hash, sum.Bytes(), encode)
	}
}
