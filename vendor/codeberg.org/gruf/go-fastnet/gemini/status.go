package gemini

// Gemini status header codes
const (
	StatusInput                          = 10
	StatusSensitive                      = 11
	StatusSuccess                        = 20
	StatusTemporaryRedirect              = 30
	StatusPermanentRedirect              = 31
	StatusTemporaryFailure               = 40
	StatusServerUnavailable              = 41
	StatusCGIError                       = 42
	StatusProxyError                     = 43
	StatusSlowDown                       = 44
	StatusPermanentFailure               = 50
	StatusNotFound                       = 51
	StatusGone                           = 52
	StatusProxyRequestRefused            = 53
	StatusBadRequest                     = 59
	StatusClientCertificateRequired      = 60
	StatusClientCertificateNotAuthorized = 61
	StatusCertificateNotValid            = 62
)

// StatusText returns string representation of supplied status code, or empty
func StatusText(code int) string {
	if code >= len(statusStrs) {
		return ""
	}
	return statusStrs[code]
}

// status code strings
var statusStrs = [63]string{
	StatusInput:                          "Input",
	StatusSensitive:                      "Sensitive",
	StatusSuccess:                        "Success",
	StatusTemporaryRedirect:              "Temporary Redirect",
	StatusPermanentRedirect:              "Permanent Redirect",
	StatusTemporaryFailure:               "Temporary Failure",
	StatusServerUnavailable:              "Server Unavailable",
	StatusCGIError:                       "CGI Error",
	StatusProxyError:                     "Proxy Error",
	StatusSlowDown:                       "Slow Down",
	StatusPermanentFailure:               "Permanent Failure",
	StatusNotFound:                       "Not Found",
	StatusGone:                           "Gone",
	StatusProxyRequestRefused:            "Proxy Request Refused",
	StatusBadRequest:                     "Bad Request",
	StatusClientCertificateRequired:      "Client Certificate Required",
	StatusClientCertificateNotAuthorized: "Client Certificate Not Authorized",
	StatusCertificateNotValid:            "Certificate Not Valid",
}
