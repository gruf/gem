package gemini

import (
	"crypto/tls"
	"fmt"

	"codeberg.org/gruf/go-bytes"
	"codeberg.org/gruf/go-errors"
	"codeberg.org/gruf/go-fastnet"
)

var (
	// preallocated scheme values
	geminiScheme = []byte("gemini")

	// preallocated error values
	errInvalidRequestHostEmpty     = errors.New("fastnet/gemini: invalid request (host empty)")
	errInvalidRequestUnknownScheme = errors.New("fastnet/gemini: invalid request (unknown scheme)")

	// preallocated response values
	rspBadRequest = []byte(fmt.Sprintf("%d Bad Request\r\n", StatusBadRequest))
)

// contextKey is our own own context key type
type contextKey string

// our constant context keys
var (
	// our custom context keys
	requestCtxKey  = contextKey("request")
	responseCtxKey = contextKey("response")
)

// Handler is a gemini specific request handler interface, use ToHandler() to convert to fastnet.Handler
type Handler interface {
	Serve(ctx *fastnet.RequestCtx, rw ResponseWriter, r *Request)
}

// ToHandler will convert a gemini Handler to fastnet.Handler
func ToHandler(h Handler) fastnet.Handler {
	if h == nil {
		return nil
	}
	return HandlerFunc(h.Serve)
}

// Handler is a gemini specific requeust handler function which implements fastnet.Handler
type HandlerFunc func(ctx *fastnet.RequestCtx, rw ResponseWriter, r *Request)

func (h HandlerFunc) Serve(ctx *fastnet.RequestCtx) {
	h(ctx, GetResponseWriter(ctx), GetRequest(ctx))
}

// ConnHandler is the gemini new connection handler function. This should be set to
// the fastnet.Server's ConnHandler member, or simply create a new server via NewServer()
func ConnHandler(cc *fastnet.ConnCtx) error {
	// Acquire bufreader for this
	rbuf := rbufPool.Get(cc.Conn())

	// Read up to first '\n'
	b, err := rbuf.ReadSlice('\n')
	if err != nil {
		// don't bother responding
		rbufPool.Put(rbuf)
		return err
	}

	// Trim any trailing '\n' / '\r'
	b = bytes.TrimByteSuffix(b, '\n')
	b = bytes.TrimByteSuffix(b, '\r')

	// Parse request URI
	err = cc.ParseURI(b)
	if err != nil {
		rbufPool.Put(rbuf)
		cc.Conn().Write(rspBadRequest)
		return err
	}

	// Host should NOT be empty
	uri := cc.Ctx().URI()
	if len(uri.Host().Bytes()) < 1 {
		rbufPool.Put(rbuf)
		cc.Conn().Write(rspBadRequest)
		return errInvalidRequestHostEmpty
	}

	// Check for gemini scheme (also accept none)
	if !bytes.Equal(uri.Scheme().Bytes(), geminiScheme) {
		rbufPool.Put(rbuf)
		cc.Conn().Write(rspBadRequest)
		return errInvalidRequestUnknownScheme
	}

	// Place new Request + Response in ctx
	state := cc.Conn().(*tls.Conn).ConnectionState()
	r := &Request{
		ctx:   cc.Ctx(),
		state: &state,
	}
	rw := &responseWriter{
		wbuf: wbufPool.Get(cc.Conn()),
	}
	cc.Ctx().WithValue(requestCtxKey, r)
	cc.Ctx().WithValue(responseCtxKey, rw)

	// Add hook to unset all bufio
	// parts of the contex, in case
	// the user hung onto it
	cc.OnDone(func() {
		rw.wbuf.Flush()
		rbufPool.Put(rbuf)
		wbufPool.Put(rw.wbuf)
		rw.wbuf = nil
	})

	return nil
}
