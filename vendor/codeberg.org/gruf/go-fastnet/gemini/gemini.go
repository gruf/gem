package gemini

import (
	"bufio"
	"crypto/tls"
	"io"
	"strconv"

	"codeberg.org/gruf/go-bytes"
	"codeberg.org/gruf/go-fastnet"
	"codeberg.org/gruf/go-uri"
)

// NewServer returns a new fastnet.Server instance for the supplied Config with gemini ConnHandler set
func NewServer() fastnet.Server {
	return fastnet.Server{
		ConnHandler: ConnHandler,
	}
}

// GetRequest fetches the stored Request object from a RequestCtx
func GetRequest(ctx *fastnet.RequestCtx) *Request {
	return ctx.Value(requestCtxKey).(*Request)
}

// Request contains information regarding a Gemini request. It is NOT valid use this
// object or any mutable data types contained within (e.g. Bytes(), StringPtr() ) after
// the handler this was originally passed to has returned. Do NOT assume it is thread-safe
type Request struct {
	ctx   *fastnet.RequestCtx
	state *tls.ConnectionState
}

// Scheme returns the scheme Bytes from the URI
func (r *Request) Scheme() bytes.Bytes {
	return r.ctx.URI().Scheme()
}

// Host returns the host Bytes from the URI
func (r *Request) Host() bytes.Bytes {
	return r.ctx.URI().Host()
}

// Path returns the path Bytes from the URI
func (r *Request) Path() bytes.Bytes {
	return r.ctx.URI().Path()
}

// Query returns the query parameters from the URI
func (r *Request) Query() *uri.QueryParams {
	return r.ctx.URI().Query()
}

// RequestURI returns a Bytes representation of the request URI (path + query)
func (r *Request) RequestURI() bytes.Bytes {
	return r.ctx.URI().RequestURI()
}

// FullURI returns a Bytes representation of the full URI (scheme + authority + path + query)
func (r *Request) FullURI() bytes.Bytes {
	return r.ctx.URI().FullURI()
}

// TLS returns the tls ConnectionState associated with this request
func (r *Request) TLS() *tls.ConnectionState {
	return r.state
}

// GetResponse fetches the Gemini Response object from a RequestCtx
func GetResponseWriter(ctx *fastnet.RequestCtx) ResponseWriter {
	return ctx.Value(responseCtxKey).(ResponseWriter)
}

// ResponseWriter defines a means of responding to a Gemini request. It is NOT valid to
// attempt writes after the handler this was originally handed to has returned. Do NOT
// assume it is thread-safe
type ResponseWriter interface {
	// Status returns the current set status code (if any)
	Status() int

	// Meta returns the currently set status meta (if any)
	Meta() string

	// WriteHeader writes the supplied status code and meta to the response Writer
	WriteHeader(code int, meta string) error

	io.Writer
	io.ReaderFrom
}

// responseWriter provides a means of responding to an Gemini request
type responseWriter struct {
	wbuf *bufio.Writer
	code int
	meta string
	have bool
}

func (rw *responseWriter) Status() int {
	return rw.code
}

func (rw *responseWriter) Meta() string {
	return rw.meta
}

func (rw *responseWriter) ReadFrom(r io.Reader) (int64, error) {
	return rw.wbuf.ReadFrom(r)
}

func (rw *responseWriter) Write(b []byte) (int, error) {
	return rw.wbuf.Write(b)
}

func (rw *responseWriter) WriteString(s string) (int, error) {
	return rw.wbuf.WriteString(s)
}

func (rw *responseWriter) WriteHeader(code int, meta string) error {
	if rw.have {
		return nil
	}
	return rw.writeHeader(code, meta)
}

func (rw *responseWriter) writeHeader(code int, meta string) error {
	rw.have = true
	rw.code = code
	rw.meta = meta
	_, err := rw.WriteString(strconv.Itoa(code) + " " + meta + "\r\n")
	return err
}
