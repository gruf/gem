package fastnet

import (
	"net"
	"time"

	"codeberg.org/gruf/go-ctx"
	"codeberg.org/gruf/go-uri"
)

// zeroTime is an empty time.Time object
var zeroTime = time.Time{}

// RequestCtx @TODO
type RequestCtx struct {
	conn net.Conn
	uri  uri.URI

	// Underlying context.Context impl
	ctx *ctx.Context
}

// WithCancel returns a cancel function for this context.
func (ctx *RequestCtx) WithCancel() func() {
	return ctx.ctx.WithCancel()
}

// Deadline implements context.Context .Deadline().
func (ctx *RequestCtx) Deadline() (deadline time.Time, ok bool) {
	return ctx.ctx.Deadline()
}

// WithDeadline updates the context deadline to supplied time, time <= Now() does nothing.
func (ctx *RequestCtx) WithDeadline(deadline time.Time) {
	ctx.ctx.WithDeadline(deadline)
}

// WithTimeout updates the context deadline to now+timeout, time <= Now() does nothing.
func (ctx *RequestCtx) WithTimeout(timeout time.Duration) {
	ctx.ctx.WithTimeout(timeout)
}

// Done implements context.Context .Done().
func (ctx *RequestCtx) Done() <-chan struct{} {
	return ctx.ctx.Done()
}

// Err implements context.Context .Err().
func (ctx *RequestCtx) Err() error {
	return ctx.ctx.Err()
}

// Value implements context.Context .Value().
func (ctx *RequestCtx) Value(key interface{}) interface{} {
	return ctx.ctx.Value(key)
}

// WithValue sets the supplied key-value pair in context data.
func (ctx *RequestCtx) WithValue(key interface{}, value interface{}) {
	ctx.ctx.WithValue(key, value)
}

// LocalAddr returns the local network address.
func (ctx *RequestCtx) LocalAddr() net.Addr {
	return ctx.conn.LocalAddr()
}

// RemoteAddr returns the remote network address.
func (ctx *RequestCtx) RemoteAddr() net.Addr {
	return ctx.conn.RemoteAddr()
}

// URI returns the parsed URI of this request context.
func (ctx *RequestCtx) URI() *uri.URI {
	return &ctx.uri
}

// ConnCtx is a structure used in building and preparing a RequestCtx for a Handler function
type ConnCtx struct {
	noCopy noCopy //nolint:unused,structcheck

	srv  *Server
	ctx  RequestCtx
	cncl func()
	hook func()
}

// Srv returns the originating fastnet Server this ConnCtx was passed from
func (cc *ConnCtx) Srv() *Server {
	return cc.srv
}

// Ctx returns the RequestCtx associated with this ConnCtx
func (cc *ConnCtx) Ctx() *RequestCtx {
	return &cc.ctx
}

// Conn returns the net.Conn to the client for this ConnCtx
func (cc *ConnCtx) Conn() net.Conn {
	return cc.ctx.conn
}

// ParseURI attempts to parse the supplied URI bytes into the RequestCtx's URI structure
func (cc *ConnCtx) ParseURI(uri []byte) error {
	return cc.ctx.URI().Parse(uri)
}

// OnDone allows you to add a function hook that is called when the ConnCtx is done
func (cc *ConnCtx) OnDone(fn func()) {
	if fn == nil {
		fn = func() {}
	}
	cc.hook = fn
}

// prepare gets this ConnCtx ready for use with supplied connection
func (cc *ConnCtx) prepare(conn net.Conn) {
	cc.ctx.conn = conn
	cc.cncl = cc.ctx.WithCancel()
}

// done is called when ConnCtx is finished, to reset the object
func (cc *ConnCtx) done() {
	// Ensure cancelled
	cc.cncl()

	// Defer in case of hook panic
	defer func() {
		// This must be done AFTER user
		// supplied hook, as their hook might
		// rely one any of these not being nil
		cc.hook = func() {}
		cc.ctx.conn = nil
		cc.ctx.ctx.Reset()
	}()

	// Call hook
	cc.hook()
}
