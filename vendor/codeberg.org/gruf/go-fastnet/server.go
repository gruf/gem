package fastnet

import (
	"context"
	"crypto/tls"
	"io"
	"net"
	"os"
	"sync"
	"time"

	"codeberg.org/gruf/go-ctx"
	"codeberg.org/gruf/go-errors"
	"codeberg.org/gruf/go-format"
	"codeberg.org/gruf/go-uri"
)

const (
	// DefaultReadBufSize is the default Server read buffer size
	DefaultReadBufSize = 4096

	// DefaultWriteBufSize is the default Serve write buffer size
	DefaultWriteBufSize = 4096
)

// ErrServerClosed is returned by Listen routines on Server.Shutdown()
var ErrServerClosed = errors.New("fastnet: server closed")

// HandlerFunc is a simple adapter to allow the use of functions as Handlers
type HandlerFunc func(*RequestCtx)

func (h HandlerFunc) Serve(ctx *RequestCtx) {
	h(ctx)
}

// Handler defines a handler for incoming requests
type Handler interface {
	Serve(*RequestCtx)
}

type Server struct {
	// Env allows setting custom server implementation variables which are then
	// accessible via the ConnCtx.Srv() within the ConnHandler
	Env map[string]interface{}

	// ReadTimeout is the total time allowed to the read the entire request.
	// Can also be used as just an initial timeout if you set the read deadline
	// again during ConnHandler to push forward the deadline for body reads. Set
	// to zero for no read timeout
	ReadTimeout time.Duration

	// WriteTimeout is the maximum duration before timing out response writes.
	// This is set once before passing to ConnHandler in case of required error
	// response, and once before Handler for the main request handling. Set to
	// zero for no write timeout
	WriteTimeout time.Duration

	// ConnHandler is the new connection handler function, a ConnCtx is passed
	// here just after accepting the new connection. Here you should implement
	// your protocol specific request parsing logic
	ConnHandler func(*ConnCtx) error

	// ReqHandler is the handler for processing incoming requests
	Handler Handler

	// LogOut is the default server error log output location
	LogOut io.Writer

	once    sync.Once       // once is used to ensure server is initialized just once
	pool    sync.Pool       // pool is used to pool-allocate RequestCtx objects
	cancel  func()          // cancel is the CancelFunc tied to the server baseCtx
	baseCtx context.Context // baseCtx is the server base Context
	serveWg sync.WaitGroup  // serveWg tracks currently running Go routines so Shutdown can wait on all to finish
}

func (srv *Server) init() {
	srv.once.Do(func() {
		// Check we've been provided handlers
		if srv.ConnHandler == nil {
			panic("fastnet: Server.ConnHandler not provided")
		}
		if srv.Handler == nil {
			panic("fastnet: Server.Handler not provided")
		}

		// Ensure a non-nil map
		if srv.Env == nil {
			srv.Env = map[string]interface{}{}
		}

		// If no log out provided, use default
		if srv.LogOut == nil {
			srv.LogOut = os.Stdout
		}

		// Setup builder pool
		srv.pool.New = func() interface{} {
			return &ConnCtx{
				srv: srv,
				ctx: RequestCtx{
					conn: nil,
					uri:  uri.New(),
					ctx:  ctx.New(),
				},
				hook: func() {},
			}
		}

		// Setup server base context with cancel
		srv.baseCtx, srv.cancel = context.WithCancel(context.Background())
	})
}

func (srv *Server) close() {
	srv.cancel()
	srv.serveWg.Wait()
}

func (srv *Server) printf(s string, a ...interface{}) {
	format.Fprintf(srv.LogOut, "fastnet: "+s, a...)
}

func (srv *Server) ListenAndServe(network, addr string) error {
	ln, err := net.Listen(network, addr)
	if err != nil {
		return err
	}
	return srv.Serve(ln)
}

func (srv *Server) ListenAndServeTLS(network, addr, certFile, keyFile string) error {
	ln, err := net.Listen(network, addr)
	if err != nil {
		return err
	}
	return srv.ServeTLS(ln, certFile, keyFile)
}

func (srv *Server) ServeTLS(ln net.Listener, certFile, keyFile string) error {
	certs, err := tls.LoadX509KeyPair(certFile, keyFile)
	if err != nil {
		return err
	}
	return srv.Serve(tls.NewListener(ln, &tls.Config{
		Certificates: []tls.Certificate{certs},
		ClientAuth:   tls.RequestClientCert,
	}))
}

func (srv *Server) Serve(ln net.Listener) error {
	srv.init()
	defer ln.Close()

	for {
		// Break-out on close
		select {
		case <-srv.baseCtx.Done():
			return ErrServerClosed
		default:
		}

		// Define these pre-loop
		var conn net.Conn
		var err error

	inner:
		for {
			// Accept next connection
			conn, err = ln.Accept()
			if err != nil {
				// Check for temporary errors
				if nErr, ok := err.(net.Error); ok && nErr.Temporary() {
					srv.printf("temporary accept error: {}", nErr)
					time.Sleep(time.Second)
					continue inner
				}

				// EOF is not an error
				if errors.Is(err, io.EOF) {
					err = nil
				}

				// Print before return
				srv.printf("accept error: {}", err)

				// Return all else
				return err
			}

			// Successful connection
			break inner
		}

		// Track upcoming
		srv.serveWg.Add(1)

		// Serve connection
		go srv.serveConn(conn)
	}
}

func (srv *Server) Shutdown() error {
	srv.close()
	return nil
}

func (srv *Server) serveConn(conn net.Conn) {
	// Prepare new ConnCtx for this connection
	connCtx := srv.pool.Get().(*ConnCtx)
	connCtx.prepare(conn)

	defer func() {
		if r := recover(); r != nil {
			// Log on any handler panics
			srv.printf("recovered panic: {}", r)
		}

		// Handle cleanup
		connCtx.done()
		conn.Close()
		srv.pool.Put(connCtx)
		srv.serveWg.Done()
	}()

	// Set connection deadlines
	if srv.ReadTimeout != 0 {
		conn.SetReadDeadline(time.Now().Add(srv.ReadTimeout))
	}
	if srv.WriteTimeout != 0 {
		// in case an error response during ConnHandler
		conn.SetWriteDeadline(time.Now().Add(srv.WriteTimeout))
	}

	// Pass onto ConnHandler for processing
	if err := srv.ConnHandler(connCtx); err != nil {
		srv.printf("conn handler error: {}", err)
		return
	}

	// Set write timeout again for actual RequestCtx handling
	if srv.WriteTimeout != 0 {
		conn.SetWriteDeadline(time.Now().Add(srv.WriteTimeout))
	}

	// Pass to request handler
	srv.Handler.Serve(&connCtx.ctx)
}
