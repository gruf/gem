package fastnet

import "io"

// NopReader provides an empty io.Reader
type NopReader struct{}

func (r *NopReader) Read([]byte) (int, error) {
	return 0, io.EOF
}

// NopReadCloser wraps an io.Reader to support io.Closer
type NopReadCloser struct{ io.Reader }

func (rc *NopReadCloser) Close() error {
	return nil
}

//nolint:unused
type noCopy struct{}

//nolint:unused
func (*noCopy) Lock() {}

//nolint:unused
func (*noCopy) Unlock() {}
