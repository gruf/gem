module codeberg.org/gruf/go-fastnet

go 1.16

require (
	codeberg.org/gruf/go-bytes v1.0.2
	codeberg.org/gruf/go-ctx v1.0.2
	codeberg.org/gruf/go-errors v1.0.5
	codeberg.org/gruf/go-fastpath v1.0.2
	codeberg.org/gruf/go-format v1.0.3
	codeberg.org/gruf/go-mimetypes v1.0.0
	codeberg.org/gruf/go-pools v1.0.2
	codeberg.org/gruf/go-uri v1.0.5
	github.com/valyala/fasthttp v1.27.0 // indirect
)
