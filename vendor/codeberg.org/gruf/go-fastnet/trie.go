package fastnet

import (
	"unicode/utf8"

	"codeberg.org/gruf/go-bytes"
	"codeberg.org/gruf/go-fastpath"
)

// GetParam attempts to fetch path parameter set by fastnet.Mux from the RequestCtx
func GetParam(ctx *RequestCtx, key string) (bytes.Bytes, bool) {
	// Look for value in ctx using our key type
	v := ctx.Value(paramKey(bytes.StringToBytes(key)))
	if v == nil {
		return nil, false
	}

	// Convert bytes bytes.Bytes type
	return bytes.ToBytes(v.([]byte)), true
}

type nodeType uint8

const (
	static   nodeType = iota
	root     nodeType = iota
	param    nodeType = iota
	catchAll nodeType = iota
)

type paramKey string

type node struct {
	noCopy noCopy //nolint:unused,structcheck

	path      []byte   // path is the path prefix for this current node
	indices   []byte   // indices
	wildChild bool     // wildChild signifies whether this node has a wildcard child
	nodeType  nodeType // nodeType specifies the type of node this is
	priority  uint32   // priority
	children  []*node  // children is a slice of child nodes of this current node
	handler   Handler  // handler is the RequestCtx Handler (if set) of the current node
}

type skipNode struct {
	path []byte
	node *node
}

// String just provides a simple means for debug printing
func (n *node) String() string {
	s := `path='` + string(n.path) + `' indices='` + string(n.indices) + `' children=`
	for i := range n.children {
		s += `{` + n.children[i].String() + `}`
	}
	return s
}

func (n *node) get(ctx *RequestCtx) (Handler, bool) {
	// Keep our own path ref we can reslice
	path := ctx.URI().Path().Bytes()

	// Use our own value for context modification,
	// still based on original request context. It
	// allows us to add param values but discard if
	// ultimately no handler is found.
	//
	// Note that when we set context values we copy
	// the node path byte slice (though unlikely it
	// will change), but just use a slice window of
	// the request path. As it's okay to make these
	// param values only valid for the length that the
	// URI path remains valid (i.e. not reset / user intervention)
	params := ([]kv)(nil)

	// Skipped allows us to track any wildcard child
	// nodes that we initially may skip past when pursuing
	// possible index matches
	var skipped *skipNode

	// isSkipped is a flag we set that allows us to skip
	// initial checks in the walk when the current node
	// ptr was set from the above 'skipped' ptr. If we
	// don't do this then we're destined to loop forever...
	var isSkipped bool

	for {
	walk:
		for {
			if !isSkipped {
				// Path shorter than prefix, no match
				if len(path) < len(n.path) {
					break walk
				}

				// Compare path up-to prefix length
				matched := bytes.Equal(path[:len(n.path)], n.path)
				if !matched {
					// length doesn't matter, no match
					break walk
				}

				// Equal length, attempt to return handler
				if len(path) == len(n.path) {
					// Found handler, set context
					if n.handler != nil {
						setParams(ctx, params)
						return n.handler, true
					}
					break walk
				}

				// At this point we know prefix matches,
				// reslice! But create slice window that
				// retains original slice start
				path = path[len(n.path):]

				// Try all non-wildcard children first
				c := path[0]
				for i := range n.indices {
					if n.indices[i] == c {
						// If current node has wildcard child,
						// store this so we can loop back to it
						if n.wildChild {
							skipped = &skipNode{
								path: path,
								node: n,
							}
						}

						n = n.children[i]
						continue walk
					}
				}

				// If no wildcard child, break
				if !n.wildChild {
					break walk
				}
			}

			// Unset skipped flag
			isSkipped = false

			// Change current node ptr to wildcard child
			n = n.children[len(n.children)-1]

			// Handle wildcard type
			switch n.nodeType {
			case param:
				// Find param contents (either up to next path
				// segment, or end of path)
				end := 0
				for end < len(path) && path[end] != '/' {
					end++
				}

				// Set request ctx parameter value
				params = append(params, kv{
					key: paramKey(n.path[1:]),
					val: path[:end],
				})

				// If we haven't hit end, keep walking
				if end < len(path) {
					// No more children, break
					if len(n.children) < 1 {
						break walk
					}

					// Reslice path, get next child
					path = path[end:]
					n = n.children[0]
					continue walk
				}

				// If handler found, update ctx & return
				if n.handler != nil {
					setParams(ctx, params)
					return n.handler, true
				}

				// Nothing found
				break walk

			case catchAll:
				// Set request ctx parameter value
				params = append(params, kv{
					key: paramKey(n.path[2:]),
					val: path,
				})

				// Handle absolutely should be set
				setParams(ctx, params)
				return n.handler, true

			default:
				panic(`invalid node type for wildcard child`)
			}
		}

		// Check if a node was skipped
		if skipped != nil {
			path = skipped.path
			n = skipped.node
			skipped = nil
			isSkipped = true
			continue
		}

		// Default value
		return nil, false
	}
}

func (n *node) add(path []byte, handler Handler) {
	pb := fastpath.Builder{}

	// Normalize the path
	pb.SetAbsolute(true)
	pb.Append(path)
	path = pb.Bytes()

	// Store full-path
	fullPath := path
	n.priority++

	// For empty tree, simply insert child
	if len(n.path) < 1 && len(n.indices) < 1 {
		n.insert(path, fullPath, handler)
		n.nodeType = root // set ourselves as root
		return
	}

walk:
	for {
		// Find longest common prefix between path and current node path
		i := longestCommonPrefix(path, n.path)

		// Longest common prefix less than current node
		// prefix, split this node
		if i < len(n.path) {
			// Create clone of this node with
			// decremented priority
			child := &node{
				path:      n.path[i:],
				indices:   n.indices,
				wildChild: n.wildChild,
				nodeType:  static,
				priority:  n.priority - 1,
				children:  n.children,
				handler:   n.handler,
			}

			// Modify current node to contain new child
			n.children = []*node{child}
			n.indices = []byte{n.path[i]}
			n.path = bytes.Copy(path[:i])
			n.handler = nil
			n.wildChild = false
		}

		// Longest common prefix less than path,
		// create a new child of this node
		if i < len(path) {
			// Reslice
			path = path[i:]
			c := path[0]

			// If next char is a '/' after a wildcard param:
			// - continue from wildcard child
			// - increment priority
			if n.nodeType == param &&
				c == '/' && len(n.children) == 1 {
				n = n.children[0]
				n.priority++
				continue walk
			}

			// Check if child with next path byte exists,
			// if so then increment this child priority and
			// switch to this child
			for i := range n.indices {
				if n.indices[i] == c {
					i = n.incrementChild(i)
					n = n.children[i]
					continue walk
				}
			}

			switch {
			// Child that is NOT a wildcard (and we aren't one)
			case c != ':' && c != '*' && n.nodeType != catchAll:
				n.indices = append(n.indices, c)
				child := &node{}
				n.addChild(child)
				n.incrementChild(len(n.indices) - 1)
				n = child

			// Already have a wildcard child
			case n.wildChild:
				// Inserting a wildcard node, need to
				// check it doesn't conflict with existing
				n = n.children[len(n.children)-1]
				n.priority++

				// Check if wildcard matches
				if len(path) >= len(n.path) && bytes.Equal(n.path, path[:len(n.path)]) &&
					// cannot add a child to a catch-all
					n.nodeType != catchAll &&
					// check for longer wildcard names
					(len(n.path) >= len(path) || path[len(n.path)] == '/') {
					continue walk
				} else {
					panic(`wildcard conflicts with existing wildcard`)
				}
			}

			n.insert(path, fullPath, handler)
			return
		}

		// Check it hasn't already been set
		if n.handler != nil {
			panic(`handler already set for current node`)
		}

		// Otherwise add handler
		n.handler = handler
		return
	}
}

func (n *node) insert(path, fullPath []byte, handler Handler) {
	for {
		// Find prefix until first wildcard
		wildcard, i := findWildcard(path)
		if i == -1 { // no wildcard
			break
		}

		switch wildcard[0] {
		// Param segment
		case ':':
			// Insert prefix before current wildcard
			if i > 0 {
				n.path = bytes.Copy(path[:i])
				path = path[i:]
			}

			// Create a new child under this node
			// to hold the wildcard
			child := &node{
				nodeType: param,
				path:     bytes.Copy(wildcard),
			}
			n.addChild(child)
			n.wildChild = true
			n = child
			n.priority++

			// If path doesn't end with this wildcard, then
			// there will be another non-wildcard subpath starting with '/'
			if len(wildcard) < len(path) {
				path = path[len(wildcard):]
				child := &node{priority: 1}
				n.addChild(child)
				n = child
				continue
			}

			// If we reached here, we're done. Insert handler in new leaf
			n.handler = handler
			return

		// Catch-all segment
		case '*':
			// Ensure at end of path
			if i+len(wildcard) < len(path) {
				panic(`catch-all routes are only permitted at the end of the path`)
			}

			// Check for conflicting root segments
			if len(n.path) > 0 && n.path[len(n.path)-1] == '/' {
				panic(`catch-all conflicts with existing root path segment`)
			}

			// Check for valid segment
			i--
			if path[i] != '/' {
				panic(`catch-all segment must be prefixed by '/' in path`)
			}

			// Set current node path
			n.path = bytes.Copy(path[:i])

			// First node: catch-all with empty path
			child := &node{
				wildChild: true,
				nodeType:  catchAll,
			}
			n.addChild(child)
			n.indices = []byte{'/'}
			n = child
			n.priority++

			// Second node: node holding the variable
			child = &node{
				path:     bytes.Copy(path[i:]),
				nodeType: catchAll,
				handler:  handler,
				priority: 1,
			}
			n.children = []*node{child}

			return
		}
	}

	// If no wildcard was found, simply insert
	n.path = bytes.Copy(path)
	n.handler = handler
}

func (n *node) addChild(child *node) {
	if n.wildChild && len(n.children) > 0 {
		// Add child at penultimate place, keep wildcard child at end
		wildChild := n.children[len(n.children)-1]
		n.children = append(n.children[:len(n.children)-1], child, wildChild)
	} else {
		// Simply append child to children slice
		n.children = append(n.children, child)
	}
}

func (n *node) incrementChild(idx int) int {
	n.children[idx].priority++
	priority := n.children[idx].priority

	// Move child at idx down the with it's new priority
	newIdx := idx
	for newIdx > 0 && n.children[newIdx-1].priority < priority {
		// Swap node positions
		n.children[newIdx-1], n.children[newIdx] = n.children[newIdx], n.children[newIdx-1]

		// Decr
		newIdx--
	}

	// If position has updated, rebuild indices
	if newIdx != idx {
		// - up to newIdx is unchanged
		// - idx to idx+1 is index char we move
		// - newIdx to idx, and idx+1 are remaining without removed char
		unchanged := n.indices[:newIdx]
		movedChar := n.indices[idx]
		remaining := append(n.indices[newIdx:idx], n.indices[idx+1:]...)
		n.indices = append(append(unchanged, movedChar), remaining...)
	}

	return newIdx
}

// findWildcard searches for wildcard segment and checks
// name for invalid characters. -1 => no wildcard found
func findWildcard(path []byte) ([]byte, int) {
	// Check for wildcard start ':' (param), '*' (wildcard)
	start := bytes.IndexAny(path, ":*")
	if start == -1 {
		return nil, -1
	}

	// Find end (check for invalid)
	i := start + 1
	for i < len(path) {
		switch path[i] {
		case '/':
			// Check for non-zero length
			if i-start < 2 {
				panic(`wildcards must have a non-empty name in path`)
			}
			return path[start:i], start
		case ':', '*':
			panic(`only one wildcard per path segment is allowed`)
		default:
			i++
		}
	}

	return path[start:], start
}

// longestCommonPrefix finds the longest prefix that both slices share
func longestCommonPrefix(a, b []byte) int {
	// Set max length
	max := len(a)
	if len(b) < max {
		max = len(b)
	}

	// Iter while bytes are similar
	i := 0
	for i < max {
		// Find next rune in each
		runeA, l := utf8.DecodeRune(a[i:])
		runeB, _ := utf8.DecodeRune(b[i:])

		// Check that runes match
		if runeA != runeB {
			return i
		}

		// Incr
		i += l
	}

	return i
}

// setParams sets provided parameter key-values in supplied RequestCtx
func setParams(ctx *RequestCtx, params []kv) {
	for i := range params {
		ctx.WithValue(
			params[i].key,
			params[i].val,
		)
	}
}

type kv struct {
	key interface{}
	val interface{}
}
