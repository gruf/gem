package ctx

// data stores context data.
type data struct {
	data []kv
}

// Get fetches value for key in data.
func (d *data) Get(key interface{}) interface{} {
	var v interface{}

	for i := range d.data {
		if d.data[i].key == key {
			v = d.data[i].val

			break
		}
	}

	return v
}

// Set will set value for key in data.
func (d *data) Set(key interface{}, value interface{}) {
	for i := range d.data {
		if d.data[i].key == key {
			// Found existing, update!
			d.data[i].val = value

			return
		}
	}

	if l := len(d.data); l < cap(d.data) {
		// Update existing KV at end of slice
		d.data = d.data[:l+1]
		d.data[l].key = key
		d.data[l].val = value
	} else {
		// Add new KV entry to slice
		d.data = append(d.data, kv{
			key: key,
			val: value,
		})
	}
}

// Reset zeros the slice length of data.
func (d *data) Reset() {
	d.data = d.data[:0]
}

// kv represents a key-value pair in data.
type kv struct {
	key interface{}
	val interface{}
}
