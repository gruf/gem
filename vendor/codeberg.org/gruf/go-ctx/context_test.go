package ctx_test

import (
	"testing"
	"time"

	"codeberg.org/gruf/go-ctx"
)

func TestContext(t *testing.T) {
	ctx := ctx.New()

	t.Logf("checking nil timer cancel")
	cncl := ctx.WithCancel()
	cncl()

	ctx.WithTimeout(time.Second)
	time.Sleep(time.Second * 2)

	t.Log(ctx.Err())
	select {
	case <-ctx.Done():
	default:
		t.Fatal("did not cancel")
	}

	t.Log("checking with value")
	ctx.WithValue("hello", "world")
	if ctx.Value("hello") != "world" {
		t.Fatal("did not store value")
	}

	t.Log("creating cancel")
	cncl = ctx.WithCancel()
	cncl()

	t.Log("resetting ctx")
	ctx.Reset()

	t.Log("checking is reset")
	if ctx.Err() != nil {
		t.Log("did not reset")
	}

	t.Log("calling ctx cancel")
	ctx.WithCancel()()

	t.Log("checking cancel err")
	if ctx.Err() == nil {
		t.Log("reset failed")
	}
}
