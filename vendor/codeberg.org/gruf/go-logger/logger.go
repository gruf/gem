package logger

import (
	"context"
	"io"
	"os"
	"sync"

	"codeberg.org/gruf/go-format"
)

type Logger struct {
	// Hooks defines a list of hooks which can be called on each
	// Entry using the .Hooks() receiver.
	Hooks []Hook

	// Format allows specifying the underlying formatting
	// method/library used.
	Format Formatter

	// Levels defines currently set mappings of available log levels
	// to their string representation.
	Levels Levels

	// Timestamp defines whether to automatically append timestamps
	// to entries written via Logger convience methods and specifically
	// Entry.TimestampIf().
	Timestamp bool

	// Level is the current log LEVEL, entries at level below the
	// currently set level will not be output.
	Level LEVEL

	// Output is the log's output writer.
	Output io.Writer

	// entry pool.
	pool *sync.Pool
}

// New returns a new Logger instance with defaults
func New(out io.Writer) *Logger {
	return NewWith(0 /* all */, true, Fmt(), 512, out)
}

// NewWith returns a new Logger instance with supplied configuration
func NewWith(lvl LEVEL, timestamp bool, fmt Formatter, bufsize int64, out io.Writer) *Logger {
	// Create new logger object
	log := &Logger{
		Format:    fmt,
		Levels:    DefaultLevels(),
		Timestamp: timestamp,
		Level:     lvl,
		Output:    out,
		pool:      &sync.Pool{},
	}

	// Ensure clock running
	startClock()

	// Set-up logger Entry pool
	log.pool.New = func() interface{} {
		return &Entry{
			lvl: unset,
			buf: &format.Buffer{B: make([]byte, 0, bufsize)},
		}
	}

	return log
}

// Entry returns a new Entry from the Logger's pool with background context.
func (l *Logger) Entry() *Entry {
	entry, _ := l.pool.Get().(*Entry)
	entry.ctx = context.Background()
	entry.log = l
	return entry
}

// release will reset an Entry and release to pool.
func (l *Logger) release(entry *Entry) {
	// Reset all
	entry.ctx = nil
	entry.buf.Reset()
	entry.lvl = unset
	entry.log = nil

	// Release to pool
	l.pool.Put(entry)
}

// New returns a new Logger cloned from current using supplied output writer,
// this is useful if you would like multiple loggers with differing outputs
// but sharing the same buffer pool. Note that any changes to Logger fields
// will ONLY affect that specific logger instance.
func (l *Logger) New(out io.Writer) *Logger {
	return &Logger{
		Format:    l.Format,
		Levels:    l.Levels,
		Timestamp: l.Timestamp,
		Level:     l.Level,
		Output:    out,
		pool:      l.pool,
	}
}

// Debug prints the provided arguments with the debug prefix.
func (l *Logger) Debug(a ...interface{}) {
	l.Log(DEBUG, a...)
}

// Debugf prints the provided format string and arguments with the debug prefix.
func (l *Logger) Debugf(s string, a ...interface{}) {
	l.Logf(DEBUG, s, a...)
}

// Info prints the provided arguments with the info prefix.
func (l *Logger) Info(a ...interface{}) {
	l.Log(INFO, a...)
}

// Infof prints the provided format string and arguments with the info prefix.
func (l *Logger) Infof(s string, a ...interface{}) {
	l.Logf(INFO, s, a...)
}

// Warn prints the provided arguments with the warn prefix.
func (l *Logger) Warn(a ...interface{}) {
	l.Log(WARN, a...)
}

// Warnf prints the provided format string and arguments with the warn prefix.
func (l *Logger) Warnf(s string, a ...interface{}) {
	l.Logf(WARN, s, a...)
}

// Error prints the provided arguments with the error prefix.
func (l *Logger) Error(a ...interface{}) {
	l.Log(ERROR, a...)
}

// Errorf prints the provided format string and arguments with the error prefix.
func (l *Logger) Errorf(s string, a ...interface{}) {
	l.Logf(ERROR, s, a...)
}

// Fatal prints provided arguments with the fatal prefix before exiting the program with os.Exit(1).
func (l *Logger) Fatal(a ...interface{}) {
	defer os.Exit(1)
	l.Log(FATAL, a...)
}

// Fatalf prints provided the provided format string and arguments with the fatal prefix before exiting the program with os.Exit(1).
func (l *Logger) Fatalf(s string, a ...interface{}) {
	defer os.Exit(1)
	l.Logf(FATAL, s, a...)
}

// Log prints the provided arguments at the supplied log level.
func (l *Logger) Log(lvl LEVEL, a ...interface{}) {
	if lvl >= l.Level {
		// Acquire new entry from pool
		entry, _ := l.pool.Get().(*Entry)
		entry.log = l

		if l.Timestamp {
			// Timestamping enabled, append time to entry
			entry.buf.AppendString(clock.NowFormat() + ` `)
		}

		// Append current log level to entry
		entry.buf.AppendString(`[` + l.Levels.Get(lvl) + `] `)

		// Append arguments to buf
		l.Format.Append(entry.buf, a...)
		entry.write() // write and release
	}
}

// Logf prints the provided format string and arguments at the supplied log level.
func (l *Logger) Logf(lvl LEVEL, s string, a ...interface{}) {
	if lvl >= l.Level {
		// Acquire new entry from pool
		entry, _ := l.pool.Get().(*Entry)
		entry.log = l

		if l.Timestamp {
			// Timestamping enabled, append time to entry
			entry.buf.AppendString(clock.NowFormat() + ` `)
		}

		// Append current log level to entry
		entry.buf.AppendString(`[` + l.Levels.Get(lvl) + `] `)

		// Append string + arguments to buf
		l.Format.Appendf(entry.buf, s, a...)
		entry.write() // write and release
	}
}

// Print simply prints provided arguments.
func (l *Logger) Print(a ...interface{}) {
	// Acquire new entry from pool
	entry, _ := l.pool.Get().(*Entry)
	entry.log = l

	if l.Timestamp {
		// Timestamping enabled, append time to entry
		entry.buf.AppendString(clock.NowFormat() + ` `)
	}

	// Append arguments to buf
	l.Format.Append(entry.buf, a...)
	entry.write() // write and release
}

// Printf simply prints provided the provided format string and arguments.
func (l *Logger) Printf(s string, a ...interface{}) {
	// Acquire new entry from pool
	entry, _ := l.pool.Get().(*Entry)
	entry.log = l

	if l.Timestamp {
		// Timestamping enabled, append time to entry
		entry.buf.AppendString(clock.NowFormat() + ` `)
	}

	// Append string + arguments to buf
	l.Format.Appendf(entry.buf, s, a...)
	entry.write() // write and release
}
