package uri

import (
	"net/url"

	"codeberg.org/gruf/go-bytes"
	"codeberg.org/gruf/go-errors"
	"codeberg.org/gruf/go-fastpath"
)

// URI parsing errors
var (
	ErrContainsCtrlByte = errors.New("uri: contains ascii ctrl byte")
	ErrInvalidScheme    = errors.New("uri: invalid scheme")
	ErrInvalidIPLiteral = errors.New("uri: invalid ip literal")
	ErrInvalidPort      = errors.New("uri: invalid port after host")
)

// URI represents a parsed URI structure
type URI struct {
	scheme   Bytes            // scheme is the URI scheme
	username Bytes            // username is the auth username portion of the URI
	password Bytes            // password is the auth password portion of the URI
	host     Bytes            // host is the host portion of the URI
	opaque   Bytes            // opaque is the opaque data portion of the URI
	path     fastpath.Builder // path is the normalized path
	pathBuf  Bytes            // pathBuf is the buffer used when escaping raw path
	rawPath  Bytes            // rawPath is the raw, un-normalized path
	rawQuery Bytes            // rawQuery is the raw query string part of the URI
	fragment Bytes            // fragment is the decoded, fragment portion of the URI

	query   QueryParams // query contains the parsed query parameters
	reqURI  Bytes       // reqURI is the the request part of the URI
	fullURI Bytes       // fullURI is the full URI

	queryParsed  bool // queryParsed defines whether query contains up-to-date params
	queryEncoded bool // queryEncoded defines whether raw query needs unescaping
}

// New returns a newly allocated URI object
func New() URI {
	return URI{
		scheme:   make([]byte, 0, 8),
		username: make([]byte, 0, 32),
		password: make([]byte, 0, 32),
		host:     make([]byte, 0, 32),
		opaque:   make([]byte, 0, 32),
		path:     fastpath.NewBuilder(make([]byte, 0, 32)),
		pathBuf:  make([]byte, 0, 32),
		rawPath:  make([]byte, 0, 32),
		rawQuery: make([]byte, 0, 32),
		fragment: make([]byte, 0, 32),

		query:   NewParams(),
		reqURI:  make([]byte, 0, 128),
		fullURI: make([]byte, 0, 128),

		queryParsed:  false,
		queryEncoded: true,
	}
}

// Parse attempts to parse supplied URI bytes into the receiving object
func (u *URI) Parse(uri []byte) error {
	// Reset before anything
	u.Reset()

	// Look for fragment in URI, unescape if so
	if n := bytes.IndexByte(uri, '#'); n != -1 {
		if err := unescape(&u.fragment, uri[n+1:]); err != nil {
			return err
		}
		uri = uri[:n]
	}

	// Now attempt to parse remaining URI
	return u.parse(uri, false)
}

// ParseString attempts to parse supplied URI string into the receiving object
func (u *URI) ParseString(uri string) error {
	return u.Parse(bytes.StringToBytes(uri))
}

func (u *URI) ParseRequest(uri []byte) error {
	// Reset before anything
	u.Reset()

	// Now attempt to parse URI
	return u.parse(uri, true)
}

func (u *URI) ParseRequestString(uri string) error {
	return u.ParseRequest(bytes.StringToBytes(uri))
}

func (u *URI) parse(uri []byte, viaRequest bool) error {
	// Ensure no ascii control bytes
	if HasAsciiControlBytes(uri) {
		return ErrContainsCtrlByte
	}

	// If first char is:
	// - valid ascii (but non-scheme) --> no scheme
	// - end of scheme char --> malformed scheme
	i := 0
	if validSchemeTable[uri[i]] == intTrue {
		// Find end of scheme
	outer:
		for i = 1; i < len(uri); i++ {
			switch {
			case validSchemeTable[uri[i]] == intTrue:
				// continue iter
			case uri[i] == ':':
				// end of scheme
				u.scheme = append(u.scheme, uri[:i]...)
				bytes.ToLower(u.scheme)
				uri = uri[i+1:]
				break outer
			default:
				// this is not scheme
				break outer
			}
		}
	} else if uri[i] == ':' {
		return ErrInvalidScheme
	}

	// Check for query string in URI
	if i = bytes.IndexByte(uri, '?'); i != -1 {
		// Check for valid query encoding
		var err error
		u.queryEncoded, err = IsValidPercentEncoding(uri[i+1:])
		if err != nil {
			return err
		}

		// Set raw query + reslice URI
		u.rawQuery = append(u.rawQuery, uri[i+1:]...)
		uri = uri[:i]
	}

	haveScheme := (len(u.scheme) > 0)
	if bytes.HasBytePrefix(uri, '/') {
		// Possibilities:
		// - scheme exists AND has leading '//'
		// - NOT by request (no scheme assumed) AND has leading '//'
		//   but NOT leading '///'
		// --> then attempt to parse authority
		if (haveScheme || !viaRequest) && bytes.HasBytePrefix(uri[1:], '/') {

			// Check for 3rd leading '/'
			n := bytes.IndexByte(uri[2:], '/')
			if !viaRequest && n != 0 {
				// Skip past leading '//'
				uri = uri[2:]

				// Check where authority ends
				var host []byte
				if n == -1 {
					host = uri // host is _all_
					uri = uri[:0]
				} else {
					host = uri[:n]
					uri = uri[n:]
				}

				// Check for auth details in host
				n = bytes.LastIndexByte(host, '@')
				if n != -1 {
					auth := host[:n]
					host = host[n+1:]

					// Split by ':' to get username + pass
					n = bytes.IndexByte(auth, ':')
					if n != -1 {
						// Attempt to unescape user + pass into bufs
						if err := unescape(&u.username, auth[:n]); err != nil {
							return err
						}
						if err := unescape(&u.password, auth[n+1:]); err != nil {
							return err
						}
					} else {
						// Attempt to unescape user into buf
						if err := unescape(&u.username, auth); err != nil {
							return err
						}
					}
				}

				// Check for IPv6 literal (surrounded by [])
				if bytes.HasBytePrefix(host, '[') {
					// Ensure we have ending bracket
					i := bytes.LastIndexByte(host[1:], ']')
					if i == -1 {
						return ErrInvalidIPLiteral
					}

					// Check for valid port after literal
					colonPort := host[i+1:]
					if bytes.HasBytePrefix(colonPort, ':') && !isAsciiNumber(colonPort[1:]) {
						return ErrInvalidPort
					}

					// Look for zone identifier (%-encoded percent)
					if j := bytes.Index(host[:i], strZone); j != -1 {
						// Host unescape up to the zone identifier
						if err := hostUnescape(&u.host, host[:j]); err != nil {
							return ErrInvalidIPLiteral
						}

						// Zone unescape zone up to end of literal
						if err := zoneUnescape(&u.host, host[j:i]); err != nil {
							return ErrInvalidIPLiteral
						}

						// Remaining host will be unescaped below
						host = host[i:]
					}
				} else if i := bytes.LastIndexByte(host, ':'); i != -1 {
					// Check for valid port in host
					if !isAsciiNumber(host[i+1:]) {
						return ErrInvalidPort
					}
				}

				// Unescape host (or whatever remains) into uri buf
				if err := hostUnescape(&u.host, host); err != nil {
					return err
				}
			}
		}
	} else {
		// Rootless path w/ scheme is opaque
		if haveScheme {
			u.opaque = append(u.opaque[:0], uri...)
			return nil
		}
	}

	// All remaining URI is path, attempt to unescape path into buf
	if err := unescape(&u.pathBuf, uri); err != nil {
		return err
	}

	// Set confirmed valid raw path + normalised path
	u.rawPath = append(u.rawPath, uri...)
	u.setNormalizedPath(u.pathBuf)

	return nil
}

// CopyTo copies the current URI data to the supplied URI
func (u *URI) CopyTo(dst *URI) {
	dst.scheme = append(dst.scheme[:0], u.scheme...)
	dst.username = append(dst.username[:0], u.username...)
	dst.password = append(dst.password[:0], u.password...)
	dst.host = append(dst.host[:0], u.host...)
	dst.opaque = append(dst.opaque[:0], u.opaque...)

	dst.rawPath = append(dst.rawPath[:0], u.rawPath...)
	if len(u.pathBuf) > 0 {
		// pathBuf is set, normalise this path
		dst.pathBuf = append(dst.pathBuf[:0], u.pathBuf...)
		dst.setNormalizedPath(u.pathBuf)
	} else {
		// simply normalise the rawPath
		dst.setNormalizedPath(u.rawPath)
	}

	u.query.CopyTo(&dst.query)
	dst.rawQuery = append(dst.rawQuery[:0], u.rawQuery...)
	dst.fragment = append(dst.fragment[:0], u.fragment...)

	// these don't need to be copied
	// dst.reqURI = append(dst.reqURI[:0], u.reqURI...)
	// dst.fullURI = append(dst.fullURI[:0], u.fullURI...)

	dst.queryParsed = u.queryParsed
	dst.queryEncoded = u.queryEncoded
}

// ToURL copies the current URI data to a url.URL object valid
// for the duration that the current URI data is valid (until next parse)
func (u *URI) ToURL() url.URL {
	// If username set, create userinfo
	var user *url.Userinfo
	if u.username.Len() > 0 {
		if u.password.Len() > 0 {
			// Set username and password
			user = url.UserPassword(
				u.username.StringPtr(),
				u.password.StringPtr(),
			)
		} else {
			// Set just the username
			user = url.User(
				u.username.StringPtr(),
			)
		}
	}

	// Return newly crafted url
	return url.URL{
		Scheme:     u.scheme.StringPtr(),
		Opaque:     u.opaque.StringPtr(),
		User:       user,
		Host:       u.host.StringPtr(),
		Path:       u.path.StringPtr(),
		RawPath:    u.rawPath.StringPtr(),
		ForceQuery: false,
		RawQuery:   u.rawQuery.StringPtr(),
		Fragment:   u.fragment.StringPtr(),
	}
}

// Scheme returns the schem byte buffer
func (u *URI) Scheme() bytes.Bytes {
	return u.scheme
}

// Scheme sets the scheme byte buffer to the supplied bytes, formatted to lowercase
func (u *URI) SetScheme(b []byte) {
	u.scheme = append(u.scheme[:0], b...)
	bytes.ToLower(u.scheme)
}

// SetSchemeString sets the scheme byte buffer to the supplied string, formatted to lowercase
func (u *URI) SetSchemeString(s string) {
	u.scheme = append(u.scheme[:0], s...)
	bytes.ToLower(u.scheme)
}

// Host returns the host byte buffer
func (u *URI) Host() bytes.Bytes {
	return u.host
}

// SetHost sets the host byte buffer to the supplied bytes, formatted to lowercase
func (u *URI) SetHost(b []byte) {
	u.host = append(u.host[:0], b...)
}

// SetHostString sets the host byte buffer to the supplied string, formatted to lowercase
func (u *URI) SetHostString(s string) {
	u.host = append(u.host[:0], s...)
}

// Opaque returns the opaque byte buffer
func (u *URI) Opaque() bytes.Bytes {
	return u.opaque
}

// SetOpaque sets the opaque byte buffer to the supplied bytes
func (u *URI) SetOpaque(b []byte) {
	u.opaque = append(u.opaque[:0], b...)
}

// SetOpaqueString sets the host byte buffer to the supplied string
func (u *URI) SetOpaqueString(s string) {
	u.opaque = append(u.opaque[:0], s...)
}

// RawPath returns the raw path (i.e. not normalized) byte buffer
func (u *URI) RawPath() bytes.Bytes {
	return u.rawPath
}

// Path returns the normalized path byte buffer
func (u *URI) Path() bytes.Bytes {
	return &u.path
}

// SetPath sets the raw path byte buffer to supplied bytes, and sets
// the normalized path buffer to the normalized form of this
func (u *URI) SetPath(b []byte) {
	u.pathBuf = u.pathBuf[:0] // reset this, is only set in Parse()
	u.rawPath = append(u.rawPath[:0], b...)
	u.setNormalizedPath(u.rawPath)
}

// SetPathString sets the raw path byte buffer to supplied string, and sets
// the normalized path buffer to the normalized form of this
func (u *URI) SetPathString(s string) {
	u.pathBuf = u.pathBuf[:0] // reset this, is only set in Parse()
	u.rawPath = append(u.rawPath[:0], s...)
	u.setNormalizedPath(u.rawPath)
}

// setNormalizedPath sets the normalized path buffer
// from the current contents of the rawPath buffer
func (u *URI) setNormalizedPath(b []byte) {
	// Check for trailing '/'
	trail := bytes.HasByteSuffix(b, '/')

	// Clean path + ensure absolute
	u.path.Reset()
	u.path.SetAbsolute(true)
	u.path.Append(b)

	// Drop trailing '/..'
	n := bytes.Index(u.path.Bytes(), strSlashDotDot)
	if n != -1 {
		u.path.Truncate(u.path.Len() - n)
	}

	// Append trailing '/' if need-be
	if len(u.path.Bytes()) > 1 && trail {
		u.path.B = append(u.path.B, '/')
	}
}

// RawQuery returns the raw query byte buffer
func (u *URI) RawQuery() bytes.Bytes {
	return u.rawQuery
}

// Query returns the query parameters, parsing raw query if necessary
func (u *URI) Query() *QueryParams {
	if !u.queryParsed {
		u.query.parse(u.rawQuery, u.queryEncoded)
		u.queryParsed = true
	}
	return &u.query
}

// SetQuery sets the query byte buffer to the supplied bytes
func (u *URI) SetQuery(b []byte) {
	u.rawQuery = append(u.rawQuery[:0], b...)

	// invalidate
	u.queryEncoded = false // manually set queries are NOT encoding checked
	u.queryParsed = false
}

// SetQueryString sets the query byte buffer to the supplied string
func (u *URI) SetQueryString(s string) {
	u.rawQuery = append(u.rawQuery[:0], s...)

	// invalidate
	u.queryEncoded = false // manually set queries are NOT encoding checked
	u.queryParsed = false
}

// Fragment returns the fragment byte buffer
func (u *URI) Fragment() bytes.Bytes {
	return u.fragment
}

// SetFragment sets the fragment byte buffer to the supplied bytes
func (u *URI) SetFragment(b []byte) {
	u.fragment = append(u.fragment[:0], b...)
}

// SetFragmentString sets the fragment byte buffer to the supplied string
func (u *URI) SetFragmentString(s string) {
	u.fragment = append(u.fragment[:0], s...)
}

// Username returns the username byte buffer
func (u *URI) Username() bytes.Bytes {
	return u.username
}

// SetUsername sets the username byte buffer to the supplied bytes
func (u *URI) SetUsername(b []byte) {
	u.username = append(u.username[:0], b...)
}

// SetUsernameString sets the username byte buffer to the supplied string
func (u *URI) SetUsernameString(s string) {
	u.username = append(u.username[:0], s...)
}

// Password returns the password byte buffer
func (u *URI) Password() bytes.Bytes {
	return u.password
}

// SetPassword sets the password byte buffer to the supplied bytes
func (u *URI) SetPassword(b []byte) {
	u.password = append(u.password[:0], b...)
}

// SetPasswordStrings sets the password byte buffer to the suppied string
func (u *URI) SetPasswordString(s string) {
	u.password = append(u.password[:0], s...)
}

// RequestURI returns the request URI of form:
// /{Path}?{Query}
func (u *URI) RequestURI() bytes.Bytes {
	// Reset the req URI
	u.reqURI = u.reqURI[:0]

	// Write decoded path content
	u.reqURI = append(u.reqURI, u.path.Bytes()...)

	// If query found, append
	if u.rawQuery.Len() > 0 {
		// Ensure query parsed
		if !u.queryParsed {
			u.query.parse(u.rawQuery, u.queryEncoded)
			u.queryParsed = true
		}

		// Append query bytes
		u.reqURI = append(u.reqURI, '?')
		u.reqURI = append(u.reqURI, u.query.Params().Bytes()...)
	}

	// Return ptr to buffer
	return u.reqURI
}

// FullURI returns the full URI of form:
// {Scheme}://{Host}/{Path}?{Query}#{Fragment}
// or
// {Scheme}:{Opaque}?{Query}#{Fragment}
func (u *URI) FullURI() bytes.Bytes {
	// Reset the full URI
	u.fullURI = u.fullURI[:0]

	// Write scheme content (if found)
	if len(u.scheme) > 0 {
		u.fullURI = append(u.fullURI, u.scheme...)
		u.fullURI = append(u.fullURI, ':')
	}

	if len(u.opaque) > 0 {
		// Opaque type URL, write this
		u.fullURI = append(u.fullURI, u.opaque...)
	} else {
		// Write rest of schema if needed
		if len(u.scheme) > 0 {
			u.fullURI = append(u.fullURI, strSlashSlash...)
		}

		// Add username (+password) if there
		if len(u.username) > 0 {
			u.fullURI = append(u.fullURI, u.username...)
			if len(u.password) > 0 {
				u.fullURI = append(u.fullURI, ':')
				u.fullURI = append(u.fullURI, u.password...)
			}
			u.fullURI = append(u.fullURI, '@')
		}

		// Write host
		u.fullURI = append(u.fullURI, u.host...)

		// Write decoded path content
		u.fullURI = append(u.fullURI, u.path.Bytes()...)
	}

	// If query found, append
	if u.rawQuery.Len() > 0 {
		// Ensure query parsed
		if !u.queryParsed {
			u.query.parse(u.rawQuery, u.queryEncoded)
			u.queryParsed = true
		}

		// Append query bytes
		u.fullURI = append(u.fullURI, '?')
		u.fullURI = append(u.fullURI, u.query.Params().Bytes()...)
	}

	// If fragment found, append
	if u.fragment.Len() > 0 {
		u.fullURI = append(u.fullURI, '#')
		u.fullURI = append(u.fullURI, u.fragment...)
	}

	// Return ptr to buffer
	return u.fullURI
}

func (u *URI) Reset() {
	u.scheme = u.scheme[:0]
	u.username = u.username[:0]
	u.password = u.password[:0]
	u.host = u.host[:0]
	u.opaque = u.opaque[:0]
	u.pathBuf = u.pathBuf[:0]
	u.path.Reset()
	u.path.SetAbsolute(true) // ensure always absolute (so rooted '/')
	u.rawPath = u.rawPath[:0]
	u.rawQuery = u.rawQuery[:0]
	u.fragment = u.fragment[:0]

	u.query.Reset()

	// these don't need resetting
	// u.reqURI = u.reqURI[:0]
	// u.fullURI = u.fullURI[:0]

	u.queryParsed = false
	u.queryEncoded = true
}
