module codeberg.org/gruf/go-uri

go 1.16

require (
	codeberg.org/gruf/go-bytes v1.0.2
	codeberg.org/gruf/go-errors v1.0.5
	codeberg.org/gruf/go-fastpath v1.0.2
	github.com/valyala/fasthttp v1.24.0
)
