package uri_test

import (
	"fmt"
	"net/url"
	"testing"

	"codeberg.org/gruf/go-bytes"
	"codeberg.org/gruf/go-fastpath"
	"codeberg.org/gruf/go-uri"
	"github.com/valyala/fasthttp"
)

var testURIs = []string{
	"http://google.com",
	"https://google.com",
	"http://www.google.com/file%20one%26two",
	"http://www.google.com/#file%20one%26two",
	"ftp://webmaster@www.google.com/",
	"ftp://john%20doe@www.google.com/",
	"http://www.google.com/?",
	"http://www.google.com/?foo=bar?",
	"http://www.google.com/?q=go+language",
	"http://www.google.com/?q=go%20language",
	"http://www.google.com/a%20b?q=c+d",
	"http:www.google.com/?q=go+language",
	"http:%2f%2fwww.google.com/?q=go+language",
	"mailto:/webmaster@golang.org",
	"mailto:webmaster@golang.org",
	"/foo?query=http://bad",
	"//foo",
	"//user@foo/path?a=b",
	"///threeslashes",
	"http://user:password@google.com",
	"http://j@ne:password@google.com",
	"http://jane:p@ssword@google.com",
	"http://j@ne:password@google.com/p@th?q=@go",
	"http://www.google.com/?q=go+language#foo",
	"http://www.google.com/?q=go+language#foo&bar",
	"http://www.google.com/?q=go+language#foo%26bar",
	"file:///home/adg/rabbits",
	"file:///C:/FooBar/Baz.txt",
	"MaIlTo:webmaster@golang.org",
	"a/b/c",
	"http://%3Fam:pa%3Fsword@google.com",
	"http://192.168.0.1/",
	"http://192.168.0.1:8080/",
	"http://[fe80::1]/",
	"http://[fe80::1]:8080/",
	"http://[fe80::1%25en0]/",
	"http://[fe80::1%25en0]:8080/",
	"http://[fe80::1%25%65%6e%301-._~]/",
	"http://[fe80::1%25%65%6e%301-._~]:8080/",
	"http://rest.rsc.io/foo%2fbar/baz%2Fquux?alt=media",
	"mysql://a,b,c/bar",
	"scheme://!$&'()*+,;=hello!:1/path",
	"http://host/!$&'()*+,;=:@[hello]",
	"http://example.com/oid/[order_id]",
	"http://192.168.0.2:8080/foo",
	"http://192.168.0.2:/foo",
	"http://2b01:e34:ef40:7730:8e70:5aff:fefe:edac:8080/foo",
	"http://2b01:e34:ef40:7730:8e70:5aff:fefe:edac:/foo",
	"http://[2b01:e34:ef40:7730:8e70:5aff:fefe:edac]:8080/foo",
	"http://[2b01:e34:ef40:7730:8e70:5aff:fefe:edac]:/foo",
	"http://hello.世界.com/foo",
	"http://hello.%e4%b8%96%e7%95%8c.com/foo",
	"http://hello.%E4%B8%96%E7%95%8C.com/foo",
	"http://example.com//foo",
	"myscheme://authority<\"hi\">/foo",
	"tcp://[2020::2020:20:2020:2020%25Windows%20Loves%20Spaces]:2020",
	"magnet:?xt=urn:btih:c12fe1c06bba254a9dc9f519b335aa7c1367a88a&dn",
	"mailto:?subject=hi",
}

func testURIParseDebugPrint(input string, url *url.URL, uri *uri.URI, urlErr, uriErr error) string {
	var urlStr string
	if url == nil {
		urlStr = "<nil>"
	} else {
		password, _ := url.User.Password()
		urlStr = fmt.Sprintf(
			`&url.URL{
	Scheme: %s,
	Host: %s,
	Opaque: %s,
	RawPath: %s,
	Path: %s,
	RawQuery: %s,
	Username: %s,
	Password: %s,
	Fragment: %s,
	RawFragment: %s,
}`,
			url.Scheme,
			url.Host,
			url.Opaque,
			url.RawPath,
			url.Path,
			url.RawQuery,
			url.User.Username(),
			password,
			url.Fragment,
			url.RawFragment,
		)
	}

	var uriStr string
	if uri == nil {
		uriStr = "<nil>"
	} else {
		uriStr = fmt.Sprintf(
			`&uri.URI{
	Scheme: %s,
	Host: %s,
	Opaque: %s,
	RawPath: %s,
	Path: %s,
	RawQuery: %s,
	Username: %s,
	Password: %s,
	Fragment: %s,
}`,
			uri.Scheme().StringPtr(),
			uri.Host().StringPtr(),
			uri.Opaque().StringPtr(),
			uri.RawPath().StringPtr(),
			uri.Path().StringPtr(),
			uri.RawQuery().StringPtr(),
			uri.Username().StringPtr(),
			uri.Password().StringPtr(),
			uri.Fragment().StringPtr(),
		)
	}

	return fmt.Sprintf(
		`input: %s

URL: %s
URL err: %v

URI: %s
URI err: %v
`,
		input,
		urlStr,
		urlErr,
		uriStr,
		uriErr,
	)
}

func testURIParse(t *testing.T, uriStr string) {
	// First parse with net/url
	expectURL, expectErr := url.Parse(uriStr)

	// Now parse with our own lib
	resultURI := uri.AcquireURI()
	resultErr := resultURI.ParseString(uriStr)
	defer uri.ReleaseURI(resultURI)

	// Double check they both err'd for same thing
	if expectErr == nil && resultErr != nil {
		t.Fatalf(
			"Expected nil error, result was not: %s",
			testURIParseDebugPrint(uriStr, expectURL, resultURI, expectErr, resultErr),
		)
	}
	if expectErr != nil && resultErr == nil {
		t.Fatalf(
			"Expected error, result was nil: %s",
			testURIParseDebugPrint(uriStr, expectURL, resultURI, expectErr, resultErr),
		)
	}

	// If expected error, return here
	if expectErr != nil {
		return
	}

	// Acquire path builder for upcoming test
	pb := fastpath.Builder{}

	// Check that both have expected values
	if expectURL.Scheme != resultURI.Scheme().StringPtr() {
		t.Fatalf(
			"Parsed schemes differ: %s",
			testURIParseDebugPrint(uriStr, expectURL, resultURI, expectErr, resultErr),
		)
	}
	if expectURL.Host != resultURI.Host().StringPtr() {
		t.Fatalf(
			"Parsed hosts differ: %s",
			testURIParseDebugPrint(uriStr, expectURL, resultURI, expectErr, resultErr),
		)
	}
	if expectURL.Opaque != resultURI.Opaque().StringPtr() {
		t.Fatalf(
			"Parsed opaques differ: %s",
			testURIParseDebugPrint(uriStr, expectURL, resultURI, expectErr, resultErr),
		)
	}
	pb.SetAbsolute(true)
	pb.AppendString(expectURL.Path)
	expectURL.Path = pb.StringPtr()
	if expectURL.Path != resultURI.Path().StringPtr() {
		t.Fatalf(
			"Parsed paths differ: %s",
			testURIParseDebugPrint(uriStr, expectURL, resultURI, expectErr, resultErr),
		)
	}
	if expectURL.RawQuery != resultURI.RawQuery().StringPtr() {
		t.Fatalf(
			"Parsed raw queries differ: %s",
			testURIParseDebugPrint(uriStr, expectURL, resultURI, expectErr, resultErr),
		)
	}
	if expectURL.User.Username() != resultURI.Username().StringPtr() {
		t.Fatalf(
			"Parsed usernames differ: %s",
			testURIParseDebugPrint(uriStr, expectURL, resultURI, expectErr, resultErr),
		)
	}
	password, _ := expectURL.User.Password()
	if password != resultURI.Password().StringPtr() {
		t.Fatalf(
			"Parsed passwords differ: %s",
			testURIParseDebugPrint(uriStr, expectURL, resultURI, expectErr, resultErr),
		)
	}
	if expectURL.Fragment != resultURI.Fragment().StringPtr() {
		t.Fatalf(
			"Parsed fragments differ: %s",
			testURIParseDebugPrint(uriStr, expectURL, resultURI, expectErr, resultErr),
		)
	}
}

func BenchmarkURI(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			u := uri.AcquireURI()
			defer uri.ReleaseURI(u)
			for _, uriStr := range testURIs {
				u.ParseString(uriStr)
			}
		}
	})
}

func BenchmarkURL(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			for _, uriStr := range testURIs {
				url.Parse(uriStr)
			}
		}
	})
}

func BenchmarkFastHTTP(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			uri := fasthttp.AcquireURI()
			defer fasthttp.ReleaseURI(uri)
			for _, uriStr := range testURIs {
				uri.Parse(nil, bytes.StringToBytes(uriStr))
			}
		}
	})
}

func TestURIParse(t *testing.T) {
	for _, uri := range testURIs {
		testURIParse(t, uri)
	}
}

func TestURICopyTo(t *testing.T) {
	for _, uriStr := range testURIs {
		uri1 := uri.New()
		uri2 := uri.New()

		uri1.ParseString(uriStr)
		uri1.CopyTo(&uri2)

		if uri1.FullURI().StringPtr() != uri2.FullURI().StringPtr() {
			t.Fatalf(
				"CopyTo failed for %s:\n1) %s\n2) %s",
				uriStr,
				uri1.FullURI().StringPtr(),
				uri2.FullURI().StringPtr(),
			)
		}
	}
}
