opinionated URI parsing library heavily inspired by [fasthttp](https://github.com/valyala/fasthttp)
and the standard `net/url` libraries. a fasthttp-like API, with stronger encode checking and opaque
support of `net/url`.

how is it opinionated? paths are automatically normalized by a minimal allocation path library i wrote,
so the `URI.Path()` will be the shortest possible version of the raw input, with `..` and extra `/`
removed. if you need the raw input path that is still available via the `URI.RawPath()` member.

benchmarks faster than both `net/url` and `fasthttp` -- run them yourself if you'd like to compare