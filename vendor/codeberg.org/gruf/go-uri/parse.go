package uri

import (
	"bytes"

	"codeberg.org/gruf/go-errors"
)

// Percent encode parsing errors
var (
	ErrBadPercentEncode = errors.New("uri: bad percent encoding")
	ErrShouldBeEscaped  = errors.New("uri: byte should be escaped")
)

const upperHex = "0123456789ABCDEF"

// NOTE:
// It may seem a little odd that we use the "Bytes" type for unexported
// functions using byte buffers in here, but it's easier if you just see them as
// regular byte slices. The only reason we use this type definition is to prevent
// having to cast and recast for the exported return types in `uri.go`.

// HasAsciiControlBytes returns whether a byte slice contains ASCII control bytes
func HasAsciiControlBytes(b []byte) bool {
	for i := 0; i < len(b); i++ {
		if b[i] < ' ' || b[i] == 0xf {
			return true
		}
	}
	return false
}

// IsValidPercentEncoding returns error on bad percent encoding within the
// supplied byte slice. Bool returned is whether you need to escape the slice.
// This only checks for valid path percent-encoding
func IsValidPercentEncoding(b []byte) (bool, error) {
	count := 0
	for i := 0; i < len(b); {
		switch b[i] {
		case '%':
			// If not a valid % encoded hex value, return with error
			if i+2 >= len(b) || isHexTable[b[i+1]] == intFalse || isHexTable[b[i+2]] == intFalse {
				return false, ErrBadPercentEncode
			}

			// Skip iteration past confirmed hex
			i += 3

			// Incr count
			count++
		default:
			// Iter
			i++
		}
	}
	return count != 0, nil
}

func HostUnescape(into *[]byte, b []byte) error {
	return hostUnescape((*Bytes)(into), b)
}

func ZoneUnescape(into *[]byte, b []byte) error {
	return zoneUnescape((*Bytes)(into), b)
}

func AuthUnescape(into *[]byte, b []byte) error {
	return PathUnescape(into, b)
}

func PathUnescape(into *[]byte, b []byte) error {
	return unescape((*Bytes)(into), b)
}

func QueryUnescape(into *[]byte, b []byte) error {
	return queryUnescape((*Bytes)(into), b)
}

func FragmentUnescape(into *[]byte, b []byte) error {
	return PathUnescape(into, b)
}

func PathEscape(into *[]byte, b []byte) {
	pathEscape((*Bytes)(into), b)
}

func QueryEscape(into *[]byte, b []byte) {
	queryEscape((*Bytes)(into), b)
}

// hostUnescape percent unescapes supplied host in 'b' into buffer 'buf', it is
// assumed that the buffer is already empty (has been reset)
func hostUnescape(buf *Bytes, b []byte) error {
	for i := 0; i < len(b); {
		switch b[i] {
		case '%':
			// If not a valid % encoded hex value, return with error
			if i+2 >= len(b) || isHexTable[b[i+1]] == intFalse || isHexTable[b[i+2]] == intFalse {
				return ErrBadPercentEncode
			}

			// In the host component % encoding can only be used
			// for non-ASCII bytes. And rfc6874 introduces %25 for
			// escaped percent sign in IPv6 literals
			if hex2intTable[b[i+1]] < 8 && !bytes.Equal(b[i:i+3], strZone) {
				return ErrBadPercentEncode
			}

			// Write hex decoded value to buf
			*buf = append(*buf, hex2intTable[b[i+1]]<<4|hex2intTable[b[i+2]])

			// Skip iteration past confirmed hex
			i += 3
		default:
			// If within ASCII range, and shoud be escaped, return error
			if b[i] < 0x80 && shouldHostEscapeTable[b[i]] == intTrue {
				return ErrShouldBeEscaped
			}

			// Write as-is
			*buf = append(*buf, b[i])

			// Iter
			i++
		}
	}
	return nil
}

// zoneUnescape percent unescapes supplied zone in 'b' into buffer 'buf', it is
// assumed that the buffer is already empty (has been reset)
func zoneUnescape(buf *Bytes, b []byte) error {
	for i := 0; i < len(b); {
		switch b[i] {
		case '%':
			// If not a valid % encoded hex value, return with error
			if i+2 >= len(b) || isHexTable[b[i+1]] == intFalse || isHexTable[b[i+2]] == intFalse {
				return ErrBadPercentEncode
			}

			// In zone encode segments, basically "anything goes".
			// We restrict %-escaped bytes here to those that are valid host name
			// bytes in their unescaped form. But then windows also puts spaces
			// here. Yay -.-
			v := hex2intTable[b[i+1]]<<4 | hex2intTable[b[i+2]]
			if v != '%' && v != ' ' && shouldHostEscapeTable[v] == intTrue {
				return ErrBadPercentEncode
			}

			// Write hex decoded value to buf
			*buf = append(*buf, hex2intTable[b[i+1]]<<4|hex2intTable[b[i+2]])

			// Skip iteration past confirmed hex
			i += 3
		default:
			// If within ASCII range, and shoud be escaped, return error
			if b[i] < 0x80 && shouldHostEscapeTable[b[i]] == intTrue {
				return ErrShouldBeEscaped
			}

			// Write as-is
			*buf = append(*buf, b[i])

			// Iter
			i++
		}
	}
	return nil
}

// unescape percent unescapes supplied path/auth/fragment in 'b' into buffer 'buf',
// it is assumed that the buffer is already empty (has been reset)
func unescape(buf *Bytes, b []byte) error {
	for i := 0; i < len(b); {
		switch b[i] {
		case '%':
			// If not a valid % encoded hex value, return with error
			if i+2 >= len(b) || isHexTable[b[i+1]] == intFalse || isHexTable[b[i+2]] == intFalse {
				return ErrBadPercentEncode
			}

			// Write hex decoded value to buf
			*buf = append(*buf, hex2intTable[b[i+1]]<<4|hex2intTable[b[i+2]])

			// Skip iteration past confirmed hex
			i += 3
		default:
			// Write as-is
			*buf = append(*buf, b[i])

			// Iter
			i++
		}
	}
	return nil
}

// queryUnescape unescapes supplied query in 'b' into buffer 'buf', it is
// assumed that the buffer is already empty (has been reset)
func queryUnescape(buf *Bytes, b []byte) error {
	for i := 0; i < len(b); {
		switch b[i] {
		case '%':
			// If not a valid % encoded hex value, return with error
			if i+2 >= len(b) || isHexTable[b[i+1]] == intFalse || isHexTable[b[i+2]] == intFalse {
				return ErrBadPercentEncode
			}

			// Write hex decoded value to buf
			*buf = append(*buf, hex2intTable[b[i+1]]<<4|hex2intTable[b[i+2]])

			// Skip iteration past confirmed hex
			i += 3
		case '+':
			// Write a ' ' instead
			*buf = append(*buf, ' ')

			// Iter
			i++
		default:
			// Write as-is
			*buf = append(*buf, b[i])

			// Iter
			i++
		}
	}
	return nil
}

func queryUnescapeNoCheck(buf *Bytes, b []byte) {
	for i := 0; i < len(b); {
		switch b[i] {
		case '%':
			// Write hex decoded value to buf
			*buf = append(*buf, hex2intTable[b[i+1]]<<4|hex2intTable[b[i+2]])

			// Skip iteration past confirmed hex
			i += 3
		case '+':
			// Write a ' ' instead
			*buf = append(*buf, ' ')

			// Iter
			i++
		default:
			// Write as-is
			*buf = append(*buf, b[i])

			// Iter
			i++
		}
	}
}

// pathEscape escapes supplied path in 'b' into buffer 'buf', it is
// assumed that the buffer is already empty (has been reset)
func pathEscape(buf *Bytes, b []byte) {
	for i := 0; i < len(b); i++ {
		switch {
		case shouldPathEscapeTable[b[i]] == intTrue:
			*buf = append(*buf, '%', upperHex[b[i]>>4], upperHex[b[i]&15])
		default:
			*buf = append(*buf, b[i])
		}
	}
}

// queryEscape escapes supplied query in 'b' into buffer 'buf', it is
// assumed that the buffer is already empty (has been reset)
func queryEscape(buf *Bytes, b []byte) {
	for i := 0; i < len(b); i++ {
		switch {
		case b[i] == '+':
			*buf = append(*buf, ' ')
		case shouldPathEscapeTable[b[i]] == intTrue:
			*buf = append(*buf, '%', upperHex[b[i]>>4], upperHex[b[i]&15])
		default:
			*buf = append(*buf, b[i])
		}
	}
}

// isAsciiNumber checks if a byte sequence is a valid ascii number
func isAsciiNumber(b []byte) bool {
	for i := range b {
		if isAsciiNumberTable[b[i]] != intTrue {
			return false
		}
	}
	return true
}
