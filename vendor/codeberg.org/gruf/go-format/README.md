# go-format

String formatting package using Rust-style formatting directives.

Output is generally more visually-friendly than `"fmt"`, while performance is neck-and-neck.

README is WIP.

## todos

- improved verbose + options for printing of number types

- more test cases

- improved verbose printing of string ptr types

