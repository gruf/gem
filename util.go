package main

import (
	"crypto/x509"
	"encoding/pem"
	"errors"
	"os"

	"codeberg.org/gruf/go-uri"
)

// isValidHostname is a simple check that we've been provided a valid hostname
func isValidHostname(hostname string) bool {
	u := uri.AcquireURI()
	defer uri.ReleaseURI(u)
	err := u.ParseString(hostname)
	return err != nil || len(u.Scheme().Bytes()) > 0 ||
		len(u.Path().Bytes()) > 0 || len(u.Query().Params().Bytes()) > 0 ||
		len(u.Fragment().Bytes()) > 0 || len(u.Opaque().Bytes()) > 0
}

// loadTLSCert loads a TLS certificate from file
func loadTLSCert(certFile string) (*x509.Certificate, error) {
	certPEM, err := os.ReadFile(certFile)
	if err != nil {
		return nil, err
	}
	der, _ := pem.Decode(certPEM)
	if der == nil {
		return nil, errors.New("failed to parse cert PEM")
	}
	cert, err := x509.ParseCertificate(der.Bytes)
	if err != nil {
		return nil, err
	}
	return cert, nil
}
