gemini server built-on `go-fastnet/gemini` with concurrency and speed in mind

supports:
- restricted paths
- CGI/1.1 support
- only allow authorized clients by TLS hash

configuration by TOML, see `example.toml`